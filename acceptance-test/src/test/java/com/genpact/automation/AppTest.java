package com.genpact.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class AppTest {

    public HtmlUnitDriver driver;
    public String HOME_URL = "http://3.111.57.107:8082/aiops/";

    @Test
    public void testHomeTitle() {
        driver = new HtmlUnitDriver();
        driver.navigate().to(HOME_URL);
        String title = driver.getTitle();
        assertEquals("AIOps | Log in", title);
    }

    @Test
    public void testLogin() {
        HtmlUnitDriver driver = new HtmlUnitDriver(true);
        driver.get(HOME_URL);
        System.out.println(driver.getCurrentUrl() + driver.getTitle());
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.id("login")).click();
        System.out.println(driver.getCurrentUrl() + driver.getTitle());
        assertEquals("AIOps : Tech Stacks", driver.getTitle());
    }

}
