<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>AIOps : Tech Stacks</title>
</head>
<body>
<h2>AIOps Pipeline Tech Stacks</h2>
<table border="1" align="center">
  <caption>Pipeline table</caption>
  <th>
  <td>Name</td>
  <td>Description</td>
  </th>
  <c:forEach items="${products}" var="product">
    <tr>
      <td><c:out value="${product.productId}" /></td>
      <td><c:out value="${product.productName}" /></td>
      <td><c:out value="${product.productDesc}" /></td>
    </tr>
  </c:forEach>
</table>
</body>
</html>