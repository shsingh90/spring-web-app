package com.genpact.webapp.service;

import com.genpact.webapp.model.Products;

import java.util.List;

public interface ProductService {
    String SERVICE_NAME = "products";

    List<Products> getProducts();
}
