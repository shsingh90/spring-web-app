package com.genpact.webapp.service.impl;

import com.genpact.webapp.model.Products;
import com.genpact.webapp.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Override
    public List<Products> getProducts() {
        List<Products> pList = new ArrayList<Products>();
        pList.add(new Products(1, "Jenkins", "CI/CD tool"));
        pList.add(new Products(2, "JFrog Artifactory", "Repository for artifactory"));
        pList.add(new Products(3, "SonarQube", "Code quality and coverage"));
        pList.add(new Products(4, "Docker", "App running in container"));
        pList.add(new Products(5, "Selenium", "Acceptance testing"));
        pList.add(new Products(6, "Jmeter", "Load testing"));
        pList.add(new Products(7, "Trivy", "Vulnerability tests"));
        return pList;
    }
}
