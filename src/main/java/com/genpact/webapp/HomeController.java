package com.genpact.webapp;

import com.genpact.webapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = {"/", "/login", "/home"}, method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String welcomePage(Model model, @RequestParam String username, @RequestParam String password) {
        if (username.equals("admin") && password.equals("admin")) {
            model.addAttribute("products", productService.getProducts());
            return "products";
        }
        model.addAttribute("error", "Please provide correct user id and/or password");
        return "login";
    }

}
